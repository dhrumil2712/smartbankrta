import React from 'react';
import { Button, Image, View, Text, StyleSheet,Platform ,ImageBackground, Dimensions, FlatList} from 'react-native';
import { createStackNavigator, createAppContainer } from 'react-navigation'; // Version can be specified in package.json
import { TabView, SceneMap } from 'react-native-tab-view';
import { List, ListItem } from "react-native-elements";
//import HomeScreen from './components/HomeScreen'

const FirstRoute = () => (

  <View style={[styles.scene, { backgroundColor: 'white' }]}>
	
	<FlatList 
  data={[{accNo: '56479583', accName: 'Chequing'},{accNo: '9850945', accName: 'Savings'}, {accNo: '2684562', accName: 'Savings'}]} 
  renderItem={({item}) => <ListItem title={item.accName} subtitle={item.accNo} containerStyle={{ borderBottomWidth: 1 }} /> } 
  keyExtractor={item => item.accNo}  
/>
  </View>
);
const SecondRoute = () => (
  <View style={[styles.scene, { backgroundColor: 'white' }]} />
);
const ThirdRoute = () => (
  <View style={[styles.scene, { backgroundColor: 'white' }]} />
);
const FourthRoute = () => (
  <View style={[styles.scene, { backgroundColor: 'white' }]} />
);

//for image 
class LogoTitle extends React.Component {
  render() {
    return (
      <Image
        source={require('./spiro.png')}
        style={{ width: 30, height: 30, alignContent: "center" }}
      />
    );
  }
}

class WelcomeScreen extends React.Component {
  static navigationOptions = {
    headerTitle: <LogoTitle />,
    headerStyle: {
      backgroundColor: 'steelblue'
    }
  };

  render() {
    return (
      <View style={ styles.container }>
        <ImageBackground
          source={require('./Background.jpg')}
          style={styles.imageContainer}
          imageStyle={styles.image}
        > 
        <View style = {styles.box}>
        <Text style={[styles.smallText, styles.textStyle]}>Good Morning</Text>
        <Text style={[styles.largeText, styles.textStyle]}>Welcome to Smart Bank</Text>
        <View style = {{padding: 10}}><Text style={[styles.smallText, styles.textStyle]}>Customer ID</Text>
        <Text style={[styles.smallText, styles.textStyle]}>UserID</Text></View></View>
        <Button style={styles.buttonContainer}
          title="SIGN IN"
          onPress={() => {
            this.props.navigation.navigate('Home', {
            });
          }}
        > </Button>
        </ImageBackground>  
      </View>
    );
  }
}

class HomeScreen extends React.Component {
  static navigationOptions = {
    headerTitle: <LogoTitle />,
  };

  render() {
    return (
      //<View style={{ flex:1, alignItems: 'flex-start', justifyContent: 'flex-start' }}></View>
      
      <View style={ styles.container}>
      <ImageBackground
      source={require('./Background.jpg')}
      style={styles.imageContainer}
      imageStyle={styles.image}
      >
        <View style = {{padding: 20, alignItems: "center"}}>
        <Button
          title="Account Transfer"
          onPress={() => {
            this.props.navigation.navigate('FromAccount', {
            });
          }}
        />
        <Button
          title="Wire Payment"
          onPress={() => {
            this.props.navigation.navigate('FromAccount', {
            });
          }}
        />
        <Button
          title="Interac e-transfer"
          onPress={() => {
            this.props.navigation.navigate('FromAccount', {
            });
          }}
        /></View></ImageBackground>
      </View> 
    );
  }
}


class FromAccountScreen extends React.Component {
  // static navigationOptions = {
  //   headerTitle: <LogoTitle />,
  // };
  state = {
    index: 0,
    routes: [
      { key: 'first', title: 'From' },
      { key: 'second', title: 'To' },
	  { key: 'third', title: 'Amount' },
	  { key: 'fourth', title: 'Review' },
    ],
	data: [],
  };
  render() {
    return (
      <TabView
       navigationState={this.state}
       renderScene={SceneMap({
         first: FirstRoute,
         second: SecondRoute,
     third: ThirdRoute,
     fourth: FourthRoute,
       })}
       onIndexChange={index => this.setState({ index })}
       initialLayout={{ width: Dimensions.get('window').width }}
     />
   );
  }
}

class ToAccountScreen extends React.Component {
  static navigationOptions = {
    headerTitle: <LogoTitle />,
  };

  render() {
    return (
      <View style={{ flex:1, alignItems: 'center', justifyContent: 'flex-start' }}>
        <Button
          title="To Account"
          onPress={() => {
            this.props.navigation.navigate('Amount', {
            });
          }}
        />
      </View>
    );
  }
}

class AmountScreen extends React.Component {
  static navigationOptions = {
    headerTitle: <LogoTitle />,
  };

  render() {
    return (
      <View style={{ flex:1, alignItems: 'center', justifyContent: 'flex-start' }}>
        <Button
          title="Amount"
          onPress={() => {
            this.props.navigation.navigate('Customize', {
            });
          }}
        />
      </View>
    );
  }
}


class CustomizeScreen extends React.Component {
  static navigationOptions = {
    headerTitle: <LogoTitle />,
  };

  render() {
    return (
      <View style={{ flex:1, alignItems: 'center', justifyContent: 'flex-start' }}>
        <Button
          title="Customize"
          onPress={() => {
            this.props.navigation.navigate('Confirmation', {
                });
          }}
        />
      </View>
    );
  }
}



class ConfirmationScreen extends React.Component {
  static navigationOptions = {
    headerTitle: <LogoTitle />,
  };

  render() {
    return (
      <View style={{ flex:1, alignItems: 'center', justifyContent: 'flex-start' }}>
        <Button
          title="Confirmation"
          onPress={() => {
            this.props.navigation.navigate('ThankYou', {
            });
          }}
        />
      </View>
    );
  }
}

class ThankYouScreen extends React.Component {
  static navigationOptions = {
    headerTitle: <LogoTitle />,
  };

  render() {
    return (
      <View style={{ flex:1, alignItems: 'center', justifyContent: 'flex-start' }}>
        <Text>Thank you for banking with us!!</Text>
        <Button
          title="SIGN OUT"
          onPress={() => this.props.navigation.navigate('Welcome')}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'stretch',
    justifyContent: 'flex-start',
  },
  textStyle: {
    textAlign: 'center',
    fontFamily: Platform.OS === 'ios' ? 'AvenirNext-Regular' : 'Roboto',
    color: '#ffeddf'
    },
    box : { 
      paddingTop: 175
    },
    largeText: {
      fontSize: 44,
      },
   smallText: {
      fontSize: 18,
      },

      buttonContainer: {
        // flex: 2,
        // //backgroundColor: '#2E9298',
        // borderRadius: 0,
        // width : 100,
        // padding: 10,
        // shadowColor: '#000000',
        // shadowOffset: {
        //   width: 0,
        //   height: 3
        // },
        // shadowRadius: 10,
        // shadowOpacity: 0.25
      },
      imageContainer: {
        flex: 1,
        },
      image: {
        flexGrow: 1,
        width: null,
        height: null,
        resizeMode: 'cover',
        },
        
});

const RootStack = createStackNavigator(
  {
    Welcome:WelcomeScreen,
    Home: HomeScreen,
    FromAccount:FromAccountScreen,
    ToAccount:ToAccountScreen,
    Amount:AmountScreen,
    Customize:CustomizeScreen,
    Confirmation:ConfirmationScreen,
    ThankYou:ThankYouScreen,
  },
  {
    initialRouteName: 'Welcome',
    /* The header config from HomeScreen is now here */
    defaultNavigationOptions: {
      headerStyle: {
        backgroundColor: '#f4511e',
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
    },
  }
);

const AppContainer = createAppContainer(RootStack);

export default class App extends React.Component {
  render() {
    return <AppContainer />;
  }
}
